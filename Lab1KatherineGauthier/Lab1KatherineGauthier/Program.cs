﻿/******************************************************
 * Titre : Lab1KatherineGauthier
 * Fait par : Katherine Gauthier
 * Date de début : 22-01-2015
 * *****************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Lab1KatherineGauthier
{
    class Program
    {

        public static float convertirTaux()
        {
            float nbTransfert = 0;
            int uniteConversion = 0;
            string str_nbTransfert = null;
            string str_uniteConversion = null;
            

            Console.WriteLine("*******************************************");
            Console.WriteLine("Quel est votre unité de départ : ");
            str_nbTransfert = Console.ReadLine();
            nbTransfert = float.Parse(str_nbTransfert);

            //Sélection du chiffre pour le switch case, donc la conversion du chiffre
            Console.WriteLine("No 1. Bits par seconde\nNo 2. Kilobits par seconde\nNo 3. Mégabits par seconde\nNo 4. Octets par seconde\nNo 5. Kilo-octets par seconde\nNo 6. Méga-octets par seconde");
            Console.WriteLine("Veuillez faire votre choix parmis ceux ci-dessus : ");
            str_uniteConversion = Console.ReadLine();
            uniteConversion = Int32.Parse(str_uniteConversion);

            //Conversion selon le choix
            switch(uniteConversion)
            {
                case 1:
                    // bits

                    afficherTaux(nbTransfert);
                    break;

                case 2:
                    // kilobit
                    nbTransfert = nbTransfert * 1000;
                    afficherTaux(nbTransfert);
                    break;

                case 3:
                    // megabit
                    nbTransfert = nbTransfert * 1000 * 1000;
                    afficherTaux(nbTransfert);
                    break;

                case 4:
                    //octet
                    nbTransfert = nbTransfert * 8;
                    afficherTaux(nbTransfert);
                    break;

                case 5:
                    //Kilo-octet
                    nbTransfert = nbTransfert * 8 * 1000;
                    afficherTaux(nbTransfert);
                    break;

                case 6:
                    //Mega-octet
                    nbTransfert = nbTransfert * 8 * 1000 * 1000;
                    afficherTaux(nbTransfert);
                    break;

            }

            return 0 ;
        }


        public static void afficherTaux(float bit)
        {
            float kilobit = 0, megabit = 0, octet = 0, kilooctet = 0, megaoctect = 0;
            string msg = null;
            //Calculs pour l'affichage

            kilobit = bit / 1000;
            megabit = kilobit / 1000;
            octet = bit / 8;
            kilooctet = octet / 1000;
            megaoctect = kilooctet / 1000;

            //Affichage
            msg = bit + " bits par seconde\t" + kilobit + " kilobits par seconde\n" + megabit + " mégabits par seconde\t" +
                  octet + " octets par seconde\n" + kilooctet + " kilo-octets par seconde\t" + megaoctect + " méga-octets par seconde";

            Console.WriteLine(msg);
        }


        public static byte afficherCharToBin()
        {
            byte [] tabBytes = null;
            char charToConvert;
            BitArray charEnBit = null;
            StringBuilder tabEnOrdre = new StringBuilder();

            Console.WriteLine("*******************************************");
            Console.Write("Entrez le caractère que vous désirez convertir : ");
            charToConvert = (char)Console.Read();

            //Conversion
            tabBytes = BitConverter.GetBytes(charToConvert);
            charEnBit = new BitArray(tabBytes);

            //empiler les bits pour avoir l'affichage en ordre
            foreach (bool unBit in charEnBit)
            {
                if (unBit == true)
                {
                    tabEnOrdre.Insert(0,"1");
                }
                else
                {
                    tabEnOrdre.Insert(0,"0");
                }
            }

            Console.Write(tabEnOrdre);

            return 0;

        }

        public static float afficherFloat()
        {
            Console.WriteLine("*******************************************");

            return 0;
        }

        public static float compostantFloat(float signe, float exposant, float mantisse)
        {

            Console.WriteLine("*******************************************");

            return signe + exposant + mantisse;
        }

        static void Main(string[] args)
        {
            int nbOption = 0;
            string str_nbOption = null;
            float signe = 0; 
            float exposant = 0; 
            float mantisse = 0;
            Console.WriteLine("Que désirez-vous faire parmis les options suivantes ");
            Console.WriteLine("No 1. Convertir un taux de transfert\nNo 2. Afficher le code binaire d'un caractère\nNo 3. Afficher un nombre en saisissant ses valeurs\nNo 4. Afficher les composant d'un nombre \nNo 5. Fermer l'application");
            str_nbOption = Console.ReadLine();
            nbOption = int.Parse(str_nbOption);


            switch (nbOption)
            {
                case 1 :

                    convertirTaux();

                    break;
                    
                case 2:

                    afficherCharToBin();

                    break;

                case 3:

                    afficherFloat();
                    
                    break;

                case 4:

                    compostantFloat(signe, exposant, mantisse);

                    break;

                case 5:

                    Console.Clear();

                    break;
            }
            Console.ReadKey();
        }
    }
}
